CREATE DATABASE roleMaster;
USE roleMaster;

CREATE TABLE users (
	id_user INT(2) NOT NULL AUTO_INCREMENT,
	name_user VARCHAR(35) NOT NULL,
	pass_user VARCHAR(50) NOT NULL,
	type_user ENUM('player', 'master', 'admin') NOT NULL DEFAULT 'player',
	image_user VARCHAR(35) NOT NULL DEFAULT 'http://placehold.it/150x150',
	date_user DATETIME NOT NULL,
	PRIMARY KEY(id_user)
) ENGINE = InnoDB;

CREATE TABLE games (
	id_game INT(2) NOT NULL AUTO_INCREMENT,
	name_game VARCHAR(40) NOT NULL,
	year_game INT(4) NOT NULL,
	publishing_game VARCHAR(40) NOT NULL,
	type_game ENUM('rules', 'no rules', 'inactive') NOT NULL DEFAULT 'inactive',
	cover_game VARCHAR(40) NOT NULL DEFAULT 'http://placehold,it/75x100',
	web_game VARCHAR(40) NOT NULL,
	PRIMARY KEY (id_game)
) ENGINE = InnoDB;

CREATE TABLE sheets (
	id_sheet INT(2) NOT NULL AUTO_INCREMENT,
	id_user INT(2) NOT NULL,
	id_game INT(2) NOT NULL,
	dates_sheet VARCHAR(40) NOT NULL,
	PRIMARY KEY (id_sheet),
	FOREIGN KEY (id_user) REFERENCES users(id_user),
	FOREIGN KEY (id_game) REFERENCES games(id_game)
) ENGINE = InnoDB;

CREATE TABLE rounds (
	id_round INT(2) NOT NULL AUTO_INCREMENT,
	name_round VARCHAR(40) NOT NULL,
	desc_round VARCHAR(40) NOT NULL DEFAULT '',
	id_master INT(2) NOT NULL,
	id_game INT(2) NOT NULL,
	PRIMARY KEY (id_round),
	FOREIGN KEY (id_master) REFERENCES users(id_user),
	FOREIGN KEY (id_game) REFERENCES games(id_game)
) ENGINE = InnoDB;

CREATE TABLE party (
	id_round INT(2) NOT NULL,
	id_sheet INT(2) NOT NULL,
	status ENUM('waiting', 'active', 'death', 'remove') NOT NULL DEFAULT 'waiting',
	FOREIGN KEY (id_round) REFERENCES rounds(id_round),
	FOREIGN KEY (id_sheet) REFERENCES sheets(id_sheet)
) ENGINE = InnoDB;


INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Vampiro: La mascarada',
				    '1994',
				    'White Wolf',
				    'rules',
				    '../images/covers/masquerade.png',
				    'formMasquerade');

INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('7º Mar',
				    '1999',
				    'Alderac Entertainment Group',
				    'inactive',
				    '../images/covers/7mar.png',
				    'form7mar');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Hengeyokai',
				    '1998',
				    'White Wolf',
				    'inactive',
				    '../images/covers/Hengeyokai.png',
				    'formHengeyokai');

INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Alatriste',
				    '2002',
				    'Devir Iberia',
				    'inactive',
				    '../images/covers/alatriste.png',
				    'formAlatriste');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Anima: Beyond Fantasy',
				    '2005',
				    'Edge Entertainment',
				    'inactive',
				    '../images/covers/anima.png',
				    'formAnima');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Changeling: El ensueño',
				    '1995',
				    'White Wolf',
				    'inactive',
				    '../images/covers/changeling.png',
				    'formChangeling');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('La llamada de Cthulhu',
				    '1994',
				    'Chaosium',
				    'inactive',
				    '../images/covers/cthulhu.png',
				    'formCthulhu');

INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Dungeons & Dragons',
				    '1974',
				    'Wizards of the Coast',
				    'inactive',
				    '../images/covers/dandd.png',
				    'formDandD');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Vampiro: Edad oscura',
				    '1996',
				    'White Wolf',
				    'rules',
				    '../images/covers/edadOscura.png',
				    'formEdadOscura');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Estirpe de Oriente',
				    '1998',
				    'White Wolf',
				    'rules',
				    '../images/covers/estirpe.png',
				    'formEstirpe');

INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Ojos grandes',
				    '2001',
				    'Ymir',
				    'inactive',
				    '../images/covers/ojos.png',
				    'formOjosGrandes');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('Sangre y seda',
				    '2001',
				    'White Wolf',
				    'rules',
				    '../images/covers/sangreSeda.png',
				    'formSangreSeda');
INSERT INTO games (name_game,
				   year_game,
				   publishing_game,
				   type_game,
				   cover_game,
				   web_game)
			VALUES ('La tierra de los ocho millones de sueños',
				    '1998',
				    'White Wolf',
				    'inactive',
				    '../images/covers/tierra8.png',
				    'form8millones');