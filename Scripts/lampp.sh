#!/bin/bash

# Autor: Carlos Mateu
# Script con la configuración realizada en el servidor LAMPP de ArchLinux

# Instalación de los paquetes
yaourt -S apache php php-fpm mariadb mod_proxy_handler

# CONFIGURACIÓN DE PHP
# Edición del archivo /etc/php/php-fpm.conf
sed -i 's/;listen = 127.0.0.1:9000/listen = 127.0.0.1:9000/g' /etc/php/php-fpm.conf
sed -i 's/;listen.allowed_clients = 127.0.0.1/listen.allowed_clients = 127.0.0.1/g' /etc/php/php-fpm.conf
sed -i 's/listen = \/run\/php-fpm\/php-fpm.sock/;listen = \/run\/php-fpm\/php-fpm.sock/g' /etc/php/php-fpm.conf

# Agregar líneas a /etc/httpd/conf/httpd.conf
echo 'LoadModule proxy_handler_module modules/mod_proxy_handler.so' >> /etc/httpd/conf/httpd.conf
echo '<FilesMatch \.php$>' >> /etc/httpd/conf/httpd.conf
echo '    SetHandler "proxy:fcgi://127.0.0.1:9000/"' >> /etc/httpd/conf/httpd.conf
echo '</FilesMatch>' >> /etc/httpd/conf/httpd.conf
echo '<IfModule dir_module>' >> /etc/httpd/conf/httpd.conf
echo '    DirectoryIndex index.php index.html' >> /etc/httpd/conf/httpd.conf
echo '</IfModule>' >> /etc/httpd/conf/httpd.conf

# Edición de /etc/php/php.ini
sed -i 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/g' /etc/php/php.ini

# CONFIGURACIÓN DE MARIADB
# Edición de /etc/php/php.ini
sed -i 's/;extension=mysqli.so/extension=mysqli.so/g' /etc/php/php.ini
sed -i 's/;extension=mysql.so/extension=mysql.so/g' /etc/php/php.ini
sed -i 's/;extension=pdo_mysql.so/extension=pdo_mysql.so/g' /etc/php/php.ini

# Creación de enlaces blandos para llamar a mariadb por su nombre
ln -s /bin/mysql /bin/mariadb
ln -s /bin/mysqladmin /bin/mariadbadmin

# Contraseña del usuario root
mariadbadmin -u root password 'toor'

# SYSTEMCTL
systemctl enable httpd.service
systemctl enable php-fpm.service
systemctl enable mysqld.service

systemctl restart httpd.service
systemctl restart php-fpm.service
systemctl restart mysqld.service