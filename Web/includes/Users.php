<?php
class Usuarios {
	private $user;
	private $password;

	# Función para abrir la conexión
	private function openDB() {
		$this->myDB = new mysqli('localhost','root','toor', 'roleMaster');
		return $this->myDB;
	}
	
	# Función para cerrar la conexión
	private function closeDB() {
		$this->myDB->close();
	}

	# Función para establecer las variables y para escapar los caracteres de 
	# los datos que llegan por el formulario antes de introducirlos en la 
	# base de datos
	public function setUser($user, $password) {
		$this->myDB = $this->openDB();
		$this->user = $this->myDB->real_escape_string($user);
		$this->password = sha1("&&&".$this->myDB->real_escape_string($password)."!!!");
		$this->closeDB();
	}
	
	# Create a new user
	public function createUser() {
		$this->myDB = $this->openDB();
		$this->myDB->autocommit(TRUE);
		$query = "INSERT INTO users (name_user, pass_user, date_user) 
			VALUES ('$this->user', '$this->password', NOW())";
		if ($this->myDB->query($query)) {
			$_SESSION['usuario'] = $this->user;
			return True;
		}
		else {
			return False;
		}
		$this->closeDB();
	}

	# valide de user name
	public function valideUser() {
		$this->myDB = $this->openDB();
		$query ="SELECT name_user FROM users WHERE name_user = '$this->user'";
		$result = $this->myDB->query($query);
		
		if ($result = $this->myDB->query($query)) {
			# recorremos las filas devueltas
			while ($row = $result->fetch_row()) {
				if ($row[1] == $this->user) {
					die("Ya existe un usuario con ese nick");
				}
			}
		}
		$this->closeDB();
	}

	#Return true if user&password are correct
	public function logUser() {
		$this->myDB = $this->openDB();
		$query ="SELECT * FROM users WHERE name_user = '$this->user'";
		$result = $this->myDB->query($query);
		
		if ($result = $this->myDB->query($query)) {
			/* recorremos las filas devueltas */
			while ($row = $result->fetch_row()) {
				if ($row[1] == $this->user and $row[2] == $this->password) {
					$_SESSION['usuario'] == $row[1];
					return true;
				}
				else{
					return false;
				}
			}
		}
		$this->closeDB();
	}

	# list all users
	public function listUsers() {
		$this->myDB = $this->openDB();
		$query ="SELECT user FROM usuarios";
		$result = $this->myDB->query($query);
		
		if ($result = $this->myDB->query($query)) {
			return $result->fetch_all();
		}
		$this->closeDB();
	}

	# Remove a user
	public function removeUser($user, $sesion) {
		$user_tmp = $user;
		$sesion_tmp = $sesion;

		# Comprueba que el usuario no sea el mismo que tiene iniciada la sesión
		if ($sesion_tmp != $user_tmp) {
			$this->myDB = $this->openDB();
			$query ="DELETE FROM usuarios WHERE user = '$user_tmp'";
			$result = $this->myDB->query($query);
			if ($result = $this->myDB->query($query)) {
				print("El usuario se ha borrado correctamente");
				$this->closeDB();
			}
		}
		else { #Si el usuario es el mismo que tiene iniciada la sesion
			print ("No te puedes borrar a ti mismo");
		}
	}

	# search user by name
	public function searchUser($search) {
		$this->myDB = $this->openDB();
		$query ='SELECT * FROM users WHERE userName LIKE "%'.$search.'%"';
		$result = $this->myDB->query($query);
		
		if ($result = $this->myDB->query($query)) {
			return $result->fetch_all();
		}
		$this->closeDB();
	}

	public function changePass($id, $old, $new) {
		$old_temp = sha1("&&&".$old."!!!");
		$new_temp = sha1("&&&".$new."!!!");
		$this->myDB = $this->openDB();
		$query ="SELECT id_user, pass_user FROM users WHERE id_user = '$id'";
		$result = $this->myDB->query($query);
		
		if ($result = $this->myDB->query($query)) {
			# recorremos las filas devueltas
			while ($row = $result->fetch_row()) {
				if ($row[1] == $old_temp) {
					$query = 'UPDATE users SET pass_user = "'.$new_temp.'" WHERE id_user = "$id"';
					if ($this->myDB->query($query)) {
						return True;
					}
					else {
						return False;
					}
				$this->closeDB();
				}
			}
		}
		$this->closeDB();
	}

	public function changeImage($id, $image) {
		$this->myDB = $this->openDB();
		$this->myDB->autocommit(TRUE);
		$query = "UPDATE users SET image_user = '".$image."' WHERE id_user = '$id'";
		if ($this->myDB->query($query)) {
			return True;
		}
		else {
			return False;
		}
		$this->closeDB();
	}
}
?>
