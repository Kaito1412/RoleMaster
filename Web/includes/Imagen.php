<?php
class Imagen {
	private $imagen;
	private $nombre;
	private $extension;
	private $error;

	# Establece las variables
	public function setImagen($imagen) {
		$this->imagen = $imagen;
		$this->extension = explode('/', $this->imagen['type']);
		$this->extension = $this->extension[1];
		$this->nombre = substr(md5(microtime()), 1, 8); //Genera nombre aleatorio
	}

	#Función que muestra el error que se ha producido al subir la imagen
	public function verError($error) {
		$this->error = $error;
		switch ($error) {
			case '1':
				die ("El archivo subido excede la directiva upload_max_filesize 
					en php.ini");
				break;
			case '2':
				die ("El archivo subido excede la directiva MAX_FILE_SIZE 
					que fue especificada en el formulario HTML. ");
				break;
			case '3':
				die ("El archivo subido fue sólo parcialmente cargado.");
				break;
			case '6':
				die ("Ningún archivo fue subido. ");
				break;
			case '7':
				die ("Falta la carpeta temporal");
				break;
			case '8':
				die ("Una extensión de PHP detuvo la carga de archivos.");
				break;
			
			default:
				die ("Se ha producido un error desconocido");
		}
	}

	#Función que comprueba la extensión del archivo para ver si es una imagen
	public function verExtension() {
		//Establecer la extension a raiz del tipo MINE declarado
		if ($this->extension != 'jpeg' && $this->extension != 'gif' && $this->extension != 'png') {
			print("La extensión del archivo es: ".$this->extension."<br />");
			die('Extensión no permitida');
		}
	}

	#Función que mueve la imagen a la carpeta indicada y le cambia el nombre.
	public function procesaImagen() {
		$this->nombre = $this->nombre.'.'.$this->extension;
		move_uploaded_file($this->imagen['tmp_name'], '../images/avatars/'.$this->nombre);
		return '../images/avatars/'.$this->nombre;
	}
}
?>