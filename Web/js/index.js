/**
 * @author cmateu
 */

$.ajax({
        type: "POST",
        url: "php/sesion.php",
        data: "funcion=sessionUser",
        dataType: "html",
        beforeSend: function(){
            
        },
        error: function(){
            alert("error petición ajax");
        },
        success: function(data){
            if (data != 0) {
              window.stop();
              window.location.href = "pages/main";
            }
        }
    });

$(document).ready(function(){
    $('#reg_user').on('change',function(){
        var characters = $(this).val();
        if (characters.length < 4) {
            $('#userMessage').empty()
            $('#userMessage').addClass('alert alert-warning')
            $('#userMessage').append("Debe tener más de tres caractéres")
        }
        else{
            $.ajax({
                type: "POST",
                url: "php/index.php",
                data: "userRep="+characters,
                dataType: "html",
                beforeSend: function(){
                    
                },
                error: function(){
                    alert("error petición ajax");
                },
                success: function(data){
                    if (data != 0) {
                      $('#userMessage').empty()
                      $('#userMessage').addClass('alert alert-warning');
                      $('#userMessage').append("Ese usuario ya existe");
                    }
                }
            });
        }
    });
    $("#reg").click(function(){
        $("#login").hide();
	    $("#regist").show();
    });
});