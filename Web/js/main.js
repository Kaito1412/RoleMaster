$.ajax({
        type: "POST",
        url: "../php/sesion.php",
        data: "funcion=sessionUser",
        dataType: "html",
        beforeSend: function(){
            
        },
        error: function(){
            window.stop();
            window.location.href = "pages/main";
        },
        success: function(data){
            if (data == 0) {
              window.stop();
              window.location.href = "../index";
            }
        }
    });

$(document).ready(function(){
    $.ajax({
        type: "POST",
        url: "../php/main.php",
        dataType: "html",
        beforeSend: function(){
            //imagen de carga
            $("#resultado").html("<p align='center'><img src='../images/dice.gif' /></p>");
        },
        error: function(){
            alert("error petición ajax");
        },
        success: function(data){
            $("#resultado").empty();
            $("#resultado").append(data);
        }
    });

    $.ajax({
        type: "POST",
        async: false,
        url: "../php/sesion.php",
        data: "funcion=sesionType",
        dataType: "html",
        beforeSend: function(){
            
        },
        error: function(){
            alert("error petición ajax");
        },
        success: function(data){
            if (data == "master" || data == "admin") {
                $("ul.nav-pills li:first-child").after('<li><a href="newParty">Crear partida</a></li>')
            }
        }
    });
});