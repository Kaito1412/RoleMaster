$(document).ready(function(){

    $.ajax({
        type: "POST",
        url: "../php/mySheets.php",
        dataType: "html",
        beforeSend: function(){
            //imagen de carga
            $("#resultado").html("<p align='center'><img src='../images/dice.gif' /></p>");
        },
        error: function(){
            alert("error petición ajax");
        },
        success: function(data){
            $("tbody").empty();
            $("tbody").append(data);
        }
    });
});