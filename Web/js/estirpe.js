$("body > div:first-child").css("background","#b51215");
$("body").css("background","#FFF");

$( document ).ready(function() {
	
	
	//
	function showDisci(disci1, disci2, disci3) {
		var disiplinas = ["#sinClan", "#animalismo", "#fortaleza", "#protean", 
		"#auspex", "#celeridad", "#extincion", "#dementacion", "#ofuscacion",
		"#potencia", "#presencia", "#taumaturgia", "#visicidud", "#quimerismo",
		"#nigromancia", "#serpentis", "#dominacion", "#obtenebracion"];
		var showD = "";
		var hiddenD = "";

		for (var i = (disiplinas.length ); i >= 0; i--) {
			if (disiplinas[i] == disci1 || disiplinas[i] == disci2  || disiplinas[i] == disci3) {
				var showD = showD + (disiplinas[i] + ","); 
			}
			else {
				var hiddenD = hiddenD + (disiplinas[i] + ",");

			}
		}
		var showD = showD.substring(0, showD.length-1);
		var hiddenD = hiddenD.substring(0, hiddenD.length-1);

		$(hiddenD).addClass("hidden");
		$('#disciplinas input.star').rating('select', false);
		$('#disciplinas > h3 > small').html("").removeClass("rojo");
		$(showD).removeClass("hidden");
	}

	/**
	 * Según el clan seleccionado modifica el resto de la ficha
	 */
	sociales = 0;
	$('select#id_clan').on('change',function(){
    	var clan = $(this).val();
    	switch(clan){
    		case "Assamita":
            	showDisci("#auspex", "#celeridad", "#extincion")
        		break;
        	case "Brujah":
            	showDisci("#celeridad", "#presencia", "#potencia")
        		break;
        	case "Gangrel":
            	showDisci("#animalismo", "#fortaleza", "#protean")
        		break;
        	case "Giovanni":
            	showDisci("#dominacion", "#nigromancia", "#potencia")
        		break;
        	case "Lasombra":
            	showDisci("#dominacion", "#obtenebracion", "#potencia")
        		break;
        	case "Malkavian":
            	showDisci("#auspex", "#dementacion", "#ofuscacion")
        		break;
        	case "Nosferatu":
            	showDisci("#animalismo", "#ofuscacion", "#potencia");
        		break;
        	case "Ravnos":
            	showDisci("#animalismo", "#fortaleza", "#quimerismo")
        		break;
        	case "Seguidores":
            	showDisci("#ofuscacion", "#presencia", "#serpentis")
        		break;
        	case "Toreador":
            	showDisci("#auspex", "#celeridad", "#presencia")
        		break;
        	case "Tremere":
            	showDisci("#auspex", "#dominacion", "#taumaturgia")
        		break;
        	case "Tzimisce":
            	showDisci("#animalismo", "#auspex", "#visicidud")
        		break;
        	case "Ventrue":
            	showDisci("#dominacion", "#fortaleza", "#presencia")
        		break;
    		default:
    			break;
    	}
    	if (clan == "Nosferatu") {
    		$('#apariencia input.star').rating('select', false);
    		$('#apariencia input.star').rating('disable');
    		sociales = 2;
    	}
    	else {
    		$('#apariencia input.star').rating('enable');
    		$('#apariencia input.star').rating('select', 0);
    		sociales = 3;
    	}
	});
	
	$('select#id_trasfondos').on('change',function(){
    	var trasfondo = "#" + $(this).val();
    	$(trasfondo).removeClass("hidden");
	});


	//Control disciblinas
	function control(level, base, modt) {
		$(level).click(function () {
			var mod = modt || 0;
			var total = 0;
			$(level + " input[name]:checked").each(
			function(index, value) {
				total = total + parseInt($(value).val());
				if ((total - mod)> base) {
					$(level  +' > h3 > small').addClass("rojo");
				}
				else {
					$(level + ' > h3 > small').removeClass("rojo");

				}
				
			}
		);
			$(level + ' > h3 > small').html(total - mod);
			
	    });
	}

	control('#fisicos', 5, 3);
	control('#sociales', 5, sociales);
	control('#mentales', 5, 3);
	control('#talentos', 13);
	control('#tecnicas', 11);
	control('#conocimientos', 9);
	control('#disciplinas', 3);
	control('#trasfondos', 5);
});
