$("body > div:first-child").css("background","#2b463f");
$("body").css("background","#FFF");

$( document ).ready(function() {
	//
	function showDisci(disci1, disci2, disci3) {
		var disiplinas = ["#sinClan", "#animalismo", "#fortaleza", "#protean", 
		"#auspex", "#celeridad", "#extincion", "#dementacion", "#ofuscacion",
		"#potencia", "#presencia", "#taumaturgia", "#visicidud", "#quimerismo",
		"#nigromancia", "#serpentis", "#dominacion", "#obtenebracion"];
		var showD = "";
		var hiddenD = "";

		for (var i = (disiplinas.length ); i >= 0; i--) {
			if (disiplinas[i] == disci1 || 
				disiplinas[i] == disci2 || 
				disiplinas[i] == disci3) {
				var showD = showD + (disiplinas[i] + ","); 
			}
			else {
				var hiddenD = hiddenD + (disiplinas[i] + ",");

			}
		}
		var showD = showD.substring(0, showD.length-1);
		var hiddenD = hiddenD.substring(0, hiddenD.length-1);

		$(hiddenD).addClass("hidden");
		$('#disciplinas input.star').rating('select', false);
		$('#disciplinas > h3 > small').html("").removeClass("rojo");
		$(showD).removeClass("hidden");
	}

	/**
	 * Según el clan seleccionado modifica el resto de la ficha
	 */
	$('select#id_clan').on('change',function(){
    	var clan = $(this).val();
    	switch(clan){
    		case "Assamita":
            	showDisci("#auspex", "#celeridad", "#extincion")
        		break;
        	case "Brujah":
            	showDisci("#celeridad", "#presencia", "#potencia")
        		break;
        	case "Gangrel":
            	showDisci("#animalismo", "#fortaleza", "#protean")
        		break;
        	case "Giovanni":
            	showDisci("#dominacion", "#nigromancia", "#potencia")
        		break;
        	case "Lasombra":
            	showDisci("#dominacion", "#obtenebracion", "#potencia")
        		break;
        	case "Malkavian":
            	showDisci("#auspex", "#dementacion", "#ofuscacion")
        		break;
        	case "Nosferatu":
            	showDisci("#animalismo", "#ofuscacion", "#potencia");
        		break;
        	case "Ravnos":
            	showDisci("#animalismo", "#fortaleza", "#quimerismo")
        		break;
        	case "Seguidores":
            	showDisci("#ofuscacion", "#presencia", "#serpentis")
        		break;
        	case "Toreador":
            	showDisci("#auspex", "#celeridad", "#presencia")
        		break;
        	case "Tremere":
            	showDisci("#auspex", "#dominacion", "#taumaturgia")
        		break;
        	case "Tzimisce":
            	showDisci("#animalismo", "#auspex", "#visicidud")
        		break;
        	case "Ventrue":
            	showDisci("#dominacion", "#fortaleza", "#presencia")
        		break;
    		default:
    			break;
    	}
    	if (clan == "Nosferatu") {
    		$('#apariencia input.star').rating('select', false);
    		$('#apariencia input.star').rating('disable');
    		sociales = 2;
    	}
    	else {
    		$('#apariencia input.star').rating('enable');
    		$('#apariencia input.star').rating('select', 0);
    		sociales = 3;
    	}
	});
	
	// Trasfondos
	$('select#id_trasfondos').on('change',function(){
    	var trasfondo = "#" + $(this).val();
    	$(trasfondo).removeClass("hidden");
	});

	$('.erase').click(function() {
		
		$('#trasfondos input.star').rating('select', false);
		$(this).parent().parent().addClass("hidden");
	});
	//Generacion
	$('#tgeneracion').click(function () {
		gen = parseInt($('#tgeneracion input[name]:checked').val());
		$('#id_generacion').val((13 - gen) + "ª");
	});

	//Control disciblinas
	function control(level, base, modt) {
		$(level).click(function () {
			var mod = modt || 0;
			var total = 0;
			$(level + " input[name]:checked").each(
			function(index, value) {
				total = total + parseInt($(value).val());
				if ((total - mod) > base) {
					$(level  +' > h3 > small').addClass("rojo");
				}
				else {
					$(level + ' > h3 > small').removeClass("rojo");

				}
				
			}
		);
			$(level + ' > h3 > small').html(total - mod);
			
	    });
	}

	control('#fisicos', 5, 3);
	control('#sociales', 5, 3);
	control('#mentales', 5, 3);
	control('#talentos', 13);
	control('#tecnicas', 11);
	control('#conocimientos', 9);
	control('#disciplinas', 3);
	control('#trasfondos', 5);

	$('#completa').click(function(){
        $('#completa').addClass("hidden");
        $('#compacta').removeClass("hidden");
        $('.nav-tabs').hide();
        $('#datos').removeClass("tab-pane");
        $('#atributos').removeClass("tab-pane");
        $('#habilidades').removeClass("tab-pane");
        $('#ventajas').removeClass("tab-pane");
        $('#otros').removeClass("tab-pane");
    });

    $('#compacta').click(function(){
        $('#completa').removeClass("hidden");
        $('#compacta').addClass("hidden");
        $('.nav-tabs').show();
        $('#datos').addClass("tab-pane");
        $('#atributos').addClass("tab-pane");
        $('#habilidades').addClass("tab-pane");
        $('#ventajas').addClass("tab-pane");
        $('#otros').addClass("tab-pane");
    });

    $('#btnModal').click(function(){
        total = 0;
        $("input.star:checked").each(
            function(index, value) {
            total += parseInt($(value).val());
            if(total > 70) {
                $('.modal-title').empty();
                $('div.modal-body').empty();
                $('div.modal-footer').empty();
                $('.modal-title').append("No puedes asignar valores de más");
                $('div.modal-body').append("Leete bien las normas y consula con tu Master si tienes dudas");
                $('div.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>');
            }
            if (total < 70) {
                $('.modal-title').empty();
                $('div.modal-body').empty();
                $('div.modal-footer').empty();
                $('.modal-title').append("Puedes asignar más valores");
                $('div.modal-body').append("Nadie te obliga a repartir todos los puntos, puede ser una aventura, pero recuerda que no podrás modificar la ficha más adelante. ¿Estás seguro?")
                $('div.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button><button type="submit" class="btn btn-default">Enviar</button>');
            }
            if (total == 70) {
                $('.modal-title').empty();
                $('div.modal-body').empty();
                $('div.modal-footer').empty();
                $('.modal-title').append("¿Deseas guardar?");
                $('div.modal-body').append("<p>¿Estás seguro de que deseas guardar la ficha?</p><p>Ten encuenta que una vez guardada no podrás modificarla a no ser que un Master decida darte puntos de experiencia</p>")
                $('div.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button><button type="submit" class="btn btn-default">Enviar</button>');
            }
        });
        $('input[type=text]').each(function(index, element) {
            var text = $(element).val();
            if (text.length < 3) {
                $('.modal-title').empty();
                $('div.modal-body').empty();
                $('div.modal-footer').empty();
                $('.modal-title').append("Faltan valores por completar");
                $('div.modal-body').append("Revisa la ficha, es posible que hayas dejado más de un valor sin completar... ¡una ficha a medias no sirve de nada!");
                $('div.modal-footer').append('<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>');
            }
        });
    });
});
