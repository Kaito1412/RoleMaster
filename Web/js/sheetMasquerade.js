$("body > div:first-child").css("background","#2b463f");
$("body").css("background","#FFF");  

$(document).ready(function(){
    function getQueryParams(qs) {
        qs = qs.split("?")[1]
        qs = qs.split("+").join(" ");
        var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

        while (tokens = re.exec(qs)) {
            params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
        }
        return params;
    }

    var urlget = getQueryParams(document.location.href);

    $('[name=json]').val(urlget['sheet']);

    $.ajax({
        type: "POST",
        async: false,
        url: "../php/sesion.php",
        data: "funcion=sesionType",
        dataType: "html",
        beforeSend: function(){
            
        },
        error: function(){
            alert("error petición ajax");
        },
        success: function(data){
            if (data == "master" || data == "admin") {
                $("[name=exp]").prop("disabled", false)
                ;
            }
        }
    });

    
   $.ajax({
        type: "POST",
        url: "../php/sheetMasquerade.php",
        async: true,
        data: "sheet=" + urlget['sheet'] + "&funcion=ajax",
        dataType: "html",
        beforeSend: function(){

        },
        error: function(){
            alert("error petición ajax");
        },
        success: function(data){
            var json = JSON.parse(data);
            sumJson = 0;
            for (attr in json) {
                if (json[attr] != 0) {
                    $('#sinClan').addClass("hidden");
                    $('#' + attr).removeClass("hidden");
                }
                $('input[type=text][name=' + attr + ']').val(json[attr]);
                $('input.star[name=' + attr + ']').rating('select', (json[attr] - 1));
                for (i = 0; i <= json[attr]; i++) {
                    $('input[type=checkbox][name=' + attr + '][value=' + i + ']').attr('checked', true);
                }
                $('input[type=checkbox][name=' + attr + '][value=' + json[attr] + ']').attr('checked', true);
                if (json['tgeneracion'] != 0) {
                    $('[name=generacion]').val(13 - json['tgeneracion'] + "ª");
                }
                if(typeof(json[attr]) == 'number'){
                    sumJson += json[attr];
                }
            }
            if (json['exp'] == 0) {
                $('input.star').rating('readOnly', true);
            }
            else {
                $('input[name=exp]').val(json['exp']);
                $('input.star').rating('readOnly', false);
                $('#id_trasfondos').removeClass('hidden');
                $('#id_disciplinas').removeClass('hidden');
            }
            sumJson = sumJson - json['salud'] - json['sangre'] - json['VoluntadBox'];
        }
    });

    $('#completa').click(function(){
        $('#completa').addClass("hidden");
        $('#compacta').removeClass("hidden");
        $('.nav-tabs').hide();
        $('#datos').removeClass("tab-pane");
        $('#atributos').removeClass("tab-pane");
        $('#habilidades').removeClass("tab-pane");
        $('#ventajas').removeClass("tab-pane");
        $('#otros').removeClass("tab-pane");
    });

    $('#compacta').click(function(){
        $('#completa').removeClass("hidden");
        $('#compacta').addClass("hidden");
        $('.nav-tabs').show();
        $('#datos').addClass("tab-pane");
        $('#atributos').addClass("tab-pane");
        $('#habilidades').addClass("tab-pane");
        $('#ventajas').addClass("tab-pane");
        $('#otros').addClass("tab-pane");
    });

    $('select#id_trasfondos').on('change',function(){
        var trasfondo = "#" + $(this).val();
        $(trasfondo).removeClass("hidden");
    });

    $('select#id_disciplinas').on('change',function(){
        var disciplina = "#" + $(this).val();
        $(disciplina).removeClass("hidden");
    });
    $('form').click(function () {
        total = 0;
            $("input.star:checked").each(
            function(index, value) {
                total = total + parseInt($(value).val());
            }
        );
        if (total > sumJson) {
            $('input.star').rating('readOnly', true);
        }
    });
});