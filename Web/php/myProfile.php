<?php
include('../php/sesion.php');
include('../includes/Users.php');
include('../includes/Imagen.php');

if (isset($_POST['oldPass']) 
	and isset($_POST['newPass1'])
	and strlen($_POST['newPass1']) > 5
	and $_POST['newPass1'] == $_POST['newPass2']) {
		
		$usuario = new Usuarios;
		$usuario->changePass(sessionID(), $_POST['oldPass'], $_POST['newPass1']);
		header('location: ../pages/myProfile');
	}

if (strlen($_FILES['avatar']["name"]) != 0) {
	$Imagen = new Imagen;
	# Comprueba los errores al subir la imagen
	if ($_FILES['avatar']["error"] != 0) {
		$Imagen->verError($_FILES['avatar']["error"]);
	}
	$Imagen->setImagen($_FILES['avatar']);
	$Imagen->verExtension(); #Comprueba la extensión

	$conexion = new Usuarios;
	$conexion->changeImage(sessionID(), $Imagen->procesaImagen());
}   header('location: ../pages/myProfile');
?>