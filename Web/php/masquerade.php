<?php
include('../php/sesion.php');

function valideText($variable) {
	if (!isset($variable) or strlen($variable) < 3) {
		print("Te falta un text");
		#header('location: formMasquerade');
	}
}

function valideSelect($variable) {
	if (!isset($variable)) {
		print("Te falta un select");
		#header('location: formMasquerade');
	}
}

function valideRadio($variable) {
	if (isset($variable)) {
		return (int)($variable);
	}
	else {
		return 0;
	}
}

valideText($_POST["nombre"]);
$nombre = $_POST["nombre"];

valideSelect($_POST["clan"]);
$clan = $_POST["clan"];

valideSelect($_POST["naturaleza"]);
$naturaleza = $_POST["naturaleza"];

valideSelect($_POST["conducta"]);
$conducta = $_POST["conducta"];

valideText($_POST["concepto"]);
$concepto = $_POST["concepto"];

valideText($_POST["sire"]);
$sire = $_POST["sire"];

valideText($_POST["refugio"]);
$refugio = $_POST["refugio"];

$fuerza = valideRadio($_POST["fuerza"]);
$destreza = valideRadio($_POST["destreza"]);
$resistencia = valideRadio($_POST["resistencia"]);
$carisma = valideRadio($_POST["carisma"]);
$manipulacion = valideRadio($_POST["manipulacion"]);
$apariencia = valideRadio($_POST["apariencia"]);
$percepcion = valideRadio($_POST["percepcion"]);
$inteligencia = valideRadio($_POST["inteligencia"]);
$astucia = valideRadio($_POST["astucia"]);

$alerta = valideRadio($_POST["alerta"]);
$atletismo = valideRadio($_POST["atletismo"]);
$callejeo = valideRadio($_POST["callejeo"]);
$empatia = valideRadio($_POST["empatia"]);
$esquivar = valideRadio($_POST["esquivar"]);
$expresion = valideRadio($_POST["expresion"]);
$intimidacion = valideRadio($_POST["intimidacion"]);
$liderezgo = valideRadio($_POST["liderezgo"]);
$pelea = valideRadio($_POST["pelea"]);
$subterfugio = valideRadio($_POST["subterfugio"]);
$armasCC = valideRadio($_POST["armasCC"]);
$armasDeFuego = valideRadio($_POST["armasDeFuego"]);
$conducir = valideRadio($_POST["conducir"]);
$etiqueta = valideRadio($_POST["etiqueta"]);
$interpretacion = valideRadio($_POST["interpretacion"]);
$pericias = valideRadio($_POST["pericias"]);
$seguridad = valideRadio($_POST["seguridad"]);
$sigilo = valideRadio($_POST["sigilo"]);
$supervivenvia = valideRadio($_POST["supervivenvia"]);
$tratoConAnimales = valideRadio($_POST["tratoConAnimales"]);
$academicismo = valideRadio($_POST["academicismo"]);
$ciencias = valideRadio($_POST["ciencias"]);
$finanzas = valideRadio($_POST["finanzas"]);
$informatica = valideRadio($_POST["informatica"]);
$investigacion = valideRadio($_POST["investigacion"]);
$leyes = valideRadio($_POST["leyes"]);
$linguistica = valideRadio($_POST["linguistica"]);
$medicina = valideRadio($_POST["medicina"]);
$ocultismo = valideRadio($_POST["ocultismo"]);
$politica = valideRadio($_POST["politica"]);

$animalismo = valideRadio($_POST["animalismo"]);
$auspex = valideRadio($_POST["auspex"]);
$celeridad = valideRadio($_POST["celeridad"]);
$dementacion = valideRadio($_POST["dementacion"]);
$dominacion = valideRadio($_POST["dominacion"]);
$extincion = valideRadio($_POST["extincion"]);
$fortaleza = valideRadio($_POST["fortaleza"]);
$nigromancia = valideRadio($_POST["nigromancia"]);
$obtenebracion = valideRadio($_POST["obtenebracion"]);
$ofuscacion = valideRadio($_POST["ofuscacion"]);
$potencia = valideRadio($_POST["potencia"]);
$presencia = valideRadio($_POST["presencia"]);
$protean = valideRadio($_POST["protean"]);
$quimerismo = valideRadio($_POST["quimerismo"]);
$serpentis = valideRadio($_POST["serpentis"]);
$taumaturgia = valideRadio($_POST["taumaturgia"]);
$visicitud = valideRadio($_POST["visicitud"]);

$aliados = valideRadio($_POST["aliados"]);
$contactos = valideRadio($_POST["contactos"]);
$criados = valideRadio($_POST["criados"]);
$fama = valideRadio($_POST["fama"]);
$tgeneracion = valideRadio($_POST["tgeneracion"]);
$influencia = valideRadio($_POST["influencia"]);
$mentor = valideRadio($_POST["mentor"]);
$posicion = valideRadio($_POST["posicion"]);
$rebanyo = valideRadio($_POST["rebanyo"]);
$recursos = valideRadio($_POST["recursos"]);

$conciencia = valideRadio($_POST["conciencia"]);
$autocontrol = valideRadio($_POST["autocontrol"]);
$coraje = valideRadio($_POST["coraje"]);

$senda = valideRadio($_POST["senda"]);
$fuerzaVoluntad = valideRadio($_POST["fuerzaVoluntad"]);

$voluntadBox = 0;
$sangre = 0;
$salud = 0;
$exp = 0;

$json = ["nombre"=>$nombre,
		 "clan"=>$clan,
		 "naturaleza"=>$naturaleza,
		 "conducta"=>$conducta,
		 "concepto"=>$concepto,
		 "sire"=>$sire,
		 "refugio"=>$refugio,
		 "fuerza"=>$fuerza,
		 "destreza"=>$destreza,
		 "resistencia"=>$resistencia,
		 "carisma"=>$carisma,
		 "manipulacion"=>$manipulacion,
		 "apariencia"=>$apariencia,
		 "percepcion"=>$percepcion,
		 "inteligencia"=>$inteligencia,
		 "astucia"=>$astucia,
		 "alerta"=>$alerta,
		 "atletismo"=>$atletismo,
		 "callejeo"=>$callejeo,
		 "empatia"=>$empatia,
		 "esquivar"=>$esquivar,
		 "expresion"=>$expresion,
		 "intimidacion"=>$intimidacion,
		 "liderezgo"=>$liderezgo,
		 "pelea"=>$pelea,
		 "subterfugio"=>$subterfugio,
		 "armasCC"=>$armasCC,
		 "armasDeFuego"=>$armasDeFuego,
		 "conducir"=>$conducir,
		 "etiqueta"=>$etiqueta,
		 "interpretacion"=>$interpretacion,
		 "pericias"=>$pericias,
		 "seguridad"=>$seguridad,
		 "sigilo"=>$sigilo,
		 "supervivenvia"=>$supervivenvia,
		 "tratoConAnimales"=>$tratoConAnimales,
		 "academicismo"=>$academicismo,
		 "ciencias"=>$ciencias,
		 "finanzas"=>$finanzas,
		 "informatica"=>$informatica,
		 "investigacion"=>$investigacion,
		 "leyes"=>$leyes,
		 "linguistica"=>$linguistica,
		 "medicina"=>$medicina,
		 "ocultismo"=>$ocultismo,
		 "politica"=>$politica,
		 "animalismo"=>$animalismo,
		 "auspex"=>$auspex,
		 "celeridad"=>$celeridad,
		 "dementacion"=>$dementacion,
		 "dominacion"=>$dominacion,
		 "extincion"=>$extincion,
		 "fortaleza"=>$fortaleza,
		 "nigromancia"=>$nigromancia,
		 "obtenebracion"=>$obtenebracion,
		 "ofuscacion"=>$ofuscacion,
		 "potencia"=>$potencia,
		 "presencia"=>$presencia,
		 "protean"=>$protean,
		 "quimerismo"=>$quimerismo,
		 "serpentis"=>$serpentis,
		 "taumaturgia"=>$taumaturgia,
		 "visicitud"=>$visicitud,
		 "aliados"=>$aliados,
		 "contactos"=>$contactos,
		 "criados"=>$criados,
		 "fama"=>$fama,
		 "tgeneracion"=>$tgeneracion,
		 "influencia"=>$influencia,
		 "mentor"=>$mentor,
		 "posicion"=>$posicion,
		 "rebanyo"=>$rebanyo,
		 "recursos"=>$recursos,
		 "conciencia"=>$conciencia,
		 "autocontrol"=>$autocontrol,
		 "coraje"=>$coraje,
		 "senda"=>$senda,
		 "fuerzaVoluntad"=>$fuerzaVoluntad,
		 "VoluntadBox"=>$voluntadBox,
		 "sangre"=>$sangre,
		 "salud"=>$salud,
		 "exp"=>$exp,
];

$nameJSON = "../json/".substr(md5(microtime()), 1, 8).".json";
$fh = fopen($nameJSON, 'w')
      or die("Error al abrir fichero de salida");
fwrite($fh, json_encode($json,JSON_UNESCAPED_UNICODE));
fclose($fh);

$con = mysql_connect('localhost','root', 'toor');
mysql_select_db('roleMaster', $con);
      
mysql_query("INSERT INTO sheets (id_user, id_game, dates_sheet, name_sheet)
    		VALUES (".$_POST['sessionID'].", ".$_POST['idGame'].", '".$nameJSON."', '".$nombre."')",$con);

header('location: ../pages/mySheets');
?>